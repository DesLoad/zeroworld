using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Spawning;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField]
    private GameObject poolObject;

    public int amount;

    private Queue<GameObject> pool;
    [HideInInspector]
    public static ObjectPooler instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        pool = new Queue<GameObject>();
        for (int i = 0; i < amount; i++)
        {
            GameObject go = Instantiate(poolObject);
            go.SetActive(false);
            go.transform.SetParent(transform);
            pool.Enqueue(go);
        }
    }

    private void Start()
    {
        NetworkSpawnManager.RegisterSpawnHandler(NetworkSpawnManager.GetPrefabHashFromGenerator("Bullet"), (posotion, rotation) =>
        {
            return FetchObject(posotion, rotation);
        });

        NetworkSpawnManager.RegisterDestroyHandler(NetworkSpawnManager.GetPrefabHashFromGenerator("Bullet"), (networkObject) =>
        {
            PoolObject(networkObject.gameObject);
        });


    }

    private NetworkObject FetchObject (Vector3 position, Quaternion rotation)
    {
        GameObject go;

        if (pool.Count > 0)
        {
            go = pool.Dequeue();
            go.SetActive(true);
        }
        else
        {
            return null;
        }
        go.transform.position = position;
        go.transform.rotation = rotation;

        return go.GetComponent<NetworkObject>();
    }

    private NetworkObject FetchObject ()
    {
        GameObject go;

        if (pool.Count > 0)
        {
            go = pool.Dequeue();
            go.SetActive(true);
        }
        else
        {
            return null;
        }

        return go.GetComponent<NetworkObject>();
    }

    private void PoolObject(GameObject go)
    {
        go.SetActive(false);
        pool.Enqueue(go);
    }

    public GameObject FetchFromPool ()
    {
        NetworkObject no = FetchObject();
        no.Spawn();

        return no.gameObject;
    }
    
    public GameObject FetchFromPoolWithOwnership (ulong netID)
    {
        NetworkObject no = FetchObject();
        no.SpawnWithOwnership(netID);

        return no.gameObject;
    }

    public void RetunrToPool(GameObject go)
    {
        PoolObject(go);
        go.GetComponent<NetworkObject>().Despawn();
    }
}
