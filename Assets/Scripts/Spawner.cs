using MLAPI;
using MLAPI.Extensions;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    NetworkObjectPool m_ObjectPool;

    [SerializeField]
    private int m_Amount;

    [SerializeField]
    private GameObject m_PowerupPrefab;
    [SerializeField]
    private GameObject m_AsteroidPrefab;
    [SerializeField]
    private GameObject m_ObstaclePrefab;

    private GameObject[] spawnPos;

    private void Start()
    {
        spawnPos = GameObject.FindGameObjectsWithTag("RuneSpawn");
    }

    private void Update()
    {
        if (!NetworkManager.Singleton.IsServer)
        {
            return;
        }

        var runeCount = GameObject.FindGameObjectsWithTag("Rune");

        //Debug.Log("Runecount = " + runeCount.Length);
        if (RuneScript.numPowerups < m_Amount)
        {
            var point = Random.Range(0, spawnPos.Length);

            for(; spawnPos[point].transform.childCount != 0; )
                    point = Random.Range(0, spawnPos.Length);

            Vector3 pos = spawnPos[point].transform.position;

            //var hits = Physics2D.OverlapCircleAll(pos, 2.0f);

            GameObject powerUp = m_ObjectPool.GetNetworkObject(m_PowerupPrefab);
            powerUp.transform.position = pos;
            powerUp.transform.parent = spawnPos[point].transform;
            powerUp.GetComponent<RuneScript>().buffType.Value = (Buff.BuffType)Random.Range(0, (int)Buff.BuffType.Last);
            powerUp.GetComponent<NetworkObject>().Spawn(null, true);
        }
    }
}
