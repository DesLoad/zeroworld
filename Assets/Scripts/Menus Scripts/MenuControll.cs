using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControll : MonoBehaviour
{
    public GameObject Class;

    public void PlayPressed()
    {
        Object.DontDestroyOnLoad(Class);
        CharacterController.classGO = Class.gameObject;
        SceneManager.LoadScene("Game",LoadSceneMode.Single);
        //SceneManager.UnloadSceneAsync("Menu");
    }

    public void ExitPressed()
    {
        Application.Quit();
    }

    public void MultiplayerPressed()
    {
        SceneManager.LoadScene("Multiplayer");
    }

    public void QuitGamePressed()
    {
        SceneManager.LoadScene("Menu");
        SceneManager.UnloadSceneAsync("Game");
    }

}
