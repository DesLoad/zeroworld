using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectChar : MonoBehaviour
{
    public string charName;
    public GameObject selectedChar;

    public GameObject textChar;
    public void CharClick()
    {
        charName = this.name;

        selectedChar = GameObject.Find("SelectedChar");
        selectedChar.GetComponent<Image>().color = Color.white;

        selectedChar.GetComponent<Image>().sprite = this.GetComponent<Image>().sprite;

        textChar = GameObject.Find("CharText");
        textChar.GetComponent<Text>().text = charName;

        CharacterController.charClass = charName;
    }
}
