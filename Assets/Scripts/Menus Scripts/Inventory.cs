using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Button btn;

    [HideInInspector]
    private Canvas canvas;
   

    private void Start()
    {
        canvas = GetComponent<Canvas>(); //��������� ���������� Canvas
        canvas.enabled = false; //���������� ��������� ��� ������
        
        btn = GetComponent<Button>();
    }

    void Update()
    {
        //btn.onClick.AddListener(QuitGame);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            canvas.enabled = !canvas.enabled;
        }
    }

    void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
