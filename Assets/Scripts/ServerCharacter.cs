using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Spawning;
using MLAPI.Configuration;
using MLAPI.Extensions;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class ServerCharacter : NetworkBehaviour
{
    public NetworkPrefab Shield;
    public NetworkPrefab Archer;

    public GameObject networkManager;

    public GameObject menus;

    private NetworkManager manager ;

    private void Start()
    {
        manager = networkManager.GetComponent<NetworkManager>();
    }


    public void ShieldApply ()
    {
        this.tag = "Shield";
    }

    public void ArcherApply ()
    {
        this.tag = "Archer";
    }

    public void ApplyButton()
    {
        this.gameObject.SetActive(false);
        menus.SetActive(true);
    }

}
