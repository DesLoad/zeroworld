using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Extensions;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


public class ServerPlayerShield : NetworkBehaviour
{
    //local data
    public float maxSpeed = 10f;        //�������� ���������
    public bool isFacingRight = true;   // ������� ��������
    public float jumpForce = 20;        // ���� ������
    private bool shieldUp = false;

    public static string charClass = "Warrior";
    private bool isGrounded = true;     //�������� �����
    public Transform groundCheck;       //����������� ������
    public float checkRadius;           //������ ��������
    public LayerMask whatIsGround;      //�� ��� �������� ����������

    private int extraJumps;              //���-�� ��� �������
    public int extraJumpsValue;          //
    public Vector3 latestPos = new Vector3(0, 0);

    public NetworkManager client;
    public int kills = 0;

    static string s_ObjectPoolTag = "ObjectPool";

    NetworkObjectPool m_ObjectPool;
    GameObject pool;

    private Animator characterAnimation;
    private Animator shieldAnimation;
    private Rigidbody2D rb;
    private GameObject scoreGO;
    private GameObject nameGO;
    private GameObject canvasGO;
    private GameObject hpInfo;
    private GameObject gameUI;
    public GameObject shieldGO;

    //Buffs Timer
    public NetworkVariableFloat SpeedBuffTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat RotateBuffTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat TripleShotTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat DoubleShotTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat QuadDamageTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat BounceTimer = new NetworkVariableFloat(0);



    //Network player
    public NetworkVariableInt Health = new NetworkVariableInt(100);
    public NetworkVariableInt Energy = new NetworkVariableInt(100);


    float rotateSpeed = 200f;
    float acceleration = 12f;
    float bulletLifetime = 2;
    float topSpeed = 7.0f;
    int m_Deaths = 0;

    public NetworkVariableString PlayerName = new NetworkVariableString("SomeGuy");



    public NetworkVariableVector3 Position = new NetworkVariableVector3(new NetworkVariableSettings
    {
        WritePermission = NetworkVariablePermission.OwnerOnly,
        ReadPermission = NetworkVariablePermission.Everyone
    });

    void Awake()
    {
        //Animation
        characterAnimation = GetComponent<Animator>();
        shieldAnimation = this.transform.GetChild(0).gameObject.GetComponent<Animator>();


        //UI
        gameUI = this.transform.GetChild(4).gameObject;
        canvasGO = this.transform.GetChild(3).gameObject;

        hpInfo = gameUI.transform.GetChild(2).gameObject;
        nameGO = canvasGO.transform.GetChild(1).gameObject;
        scoreGO = canvasGO.transform.GetChild(2).gameObject;

        
        //Other
        transform.position = new Vector2(Random.Range(-5f, 5f), 3);

        pool = GameObject.FindWithTag(s_ObjectPoolTag);
        var list = pool.GetComponents(typeof(Component));
        for (int i = 0; i < list.Length; i++)
        {
            Debug.Log(list[i].name);
        }
        m_ObjectPool = GameObject.FindWithTag(s_ObjectPoolTag).GetComponent<NetworkObjectPool>();
        if (m_ObjectPool == null)
            Debug.Log(pool.GetComponents<NetworkObjectPool>());
        Assert.IsNotNull(m_ObjectPool, $"{nameof(NetworkObjectPool)} not found in scene. Did you apply the {s_ObjectPoolTag} to the GameObject?");

        hpInfo.GetComponent<Text>().text = Health.Value.ToString();
        nameGO.GetComponent<Text>().text = name;
        scoreGO.GetComponent<Text>().text = kills.ToString() + "/" + m_Deaths.ToString();

        
    }


    public override void NetworkStart()
    {
        transform.position = new Vector3(0, 1);
    }

    private void FixedUpdate()
    {
        if (IsLocalPlayer)
        {
            MovePlayer();
        }
    }

    private void Update()
    {
        if (IsLocalPlayer)
        {
            JumpPlayer();

            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                FireServerRpc();
            }
            if (Input.GetKeyDown(KeyCode.S) && shieldUp == false)
            {
                ShieldUp();
            }
            if (Input.GetKeyUp(KeyCode.S) && shieldUp == true)
            {
                ShieldUp();
            }
        }
    }

    void MovePlayer()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        float move = Input.GetAxis("Horizontal");

        //characterAnimation.SetFloat("Speed", Mathf.Abs(move));

        rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);

        if (move > 0 && !isFacingRight)
            Flip();
        else if (move < 0 && isFacingRight)
            Flip();
    }



    void JumpPlayer()
    {
        if (isGrounded == true)
        {
            extraJumps = extraJumpsValue;
        }

        if (Input.GetKeyDown(KeyCode.Space) && (extraJumps > 0))
        {
            rb.velocity = Vector2.up * jumpForce;//������ �� ������� � ������������ �����
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    void ShieldUp()
    {
        shieldUp = !shieldUp;
        shieldGO.transform.GetChild(0).gameObject.SetActive(shieldUp);

        if (shieldUp == true)
        {

        }
    }

    void Fire()
    {
        //TODO: Sword Attack
    }

    public bool TakeDamage(int amount)
    {
        Health.Value = Health.Value - amount;
        hpInfo.GetComponent<Text>().text = Health.Value.ToString();
        if (Health.Value <= 0)
        {
            Health.Value = 0;

            //todo: reset all buffs

            m_Deaths += 1;
            Health.Value = 100;
            transform.position = new Vector2(0, 3);
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            GetComponent<Rigidbody2D>().angularVelocity = 0;
            hpInfo.GetComponent<Text>().text = Health.Value.ToString();
            return true;
        }
        return false;
    }

    void OnHealth(int oldValue, int newValue)
    {
        Health.Value = newValue;
    }

    void OnEnable()
    {
        Health.OnValueChanged += OnHealth;
    }

    void OnDisable()
    {
        Health.OnValueChanged -= OnHealth;
    }

    private void Start()
    {
        //GetComponent<NetworkObject>().ChangeOwnership(ID);
        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();
        if (IsLocalPlayer)
        {
            gameUI.SetActive(true);
        }
    }

    private void Flip()
    {
        if (!IsLocalPlayer)
        {
            return;
        }
        if (isFacingRight == true)
        {
            canvasGO.GetComponent<Canvas>().transform.eulerAngles = new Vector3(0, 180, 0);
            rb.transform.eulerAngles = new Vector3(0, 180, 0);
            isFacingRight = !isFacingRight;
        }
        else
        {
            canvasGO.GetComponent<Canvas>().transform.eulerAngles = new Vector3(0, 180, 0);
            rb.transform.eulerAngles = new Vector3(0, 0, 0);
            isFacingRight = !isFacingRight;
        }
        

        /*
        //������ ����������� �������� ���������
        
        //�������� ������� ���������
        theScale = rb.transform.eulerAngles;
        //��������� �������� ��������� �� ��� �
        theScale.y *= -1;
        //������ ����� ������ ���������, ������ �������, �� ��������� ����������
        rb.transform.eulerAngles = theScale;*/
    }


    public void ScorePlus()
    {
        kills++;

        scoreGO.GetComponent<Text>().text = kills.ToString() + "/" + m_Deaths.ToString();

        if (kills >= 15)
        {

        }
    }

    //ServerRPC

    [ServerRpc]
    public void ScorePlusServerRpc()
    {
        ScorePlus();
    }

    [ServerRpc]
    void FireServerRpc()
    {
        //var right = transform.right;
        Fire();
    }
}
