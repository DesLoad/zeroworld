using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateChunk : MonoBehaviour
{
    public GameObject DirtTile;
    public GameObject StoneTile;
    public GameObject GrassTile;
    public GameObject WaterTile;

    

    public float heightMultiplier;
    public int heightAddiction;
    public float smoothness;

    [HideInInspector]
    public float seed;
    [HideInInspector]
    public int numChunk;
    [HideInInspector]
    public int biome;
    [HideInInspector]
    public int width;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 400; i++)
        {
            Debug.Log(Mathf.RoundToInt(Mathf.PerlinNoise(seed, i) / smoothness) * heightMultiplier);
        }
        Generate();
    }

    // Update is called once per frame
    public void Generate ()
    {
        for (float i = 0; i < width; i++)
        {
            int h = Mathf.RoundToInt(Mathf.PerlinNoise(seed, (numChunk * width / smoothness) + i / smoothness) * 10 * heightMultiplier  + heightAddiction);

            for (int j = 0; j < h; j++)
            {
                GameObject selectedTile;
                if (j < h - 1)
                {
                    selectedTile = DirtTile;
                }
                else
                {
                    selectedTile = GrassTile;
                }

                GameObject newTile = Instantiate(selectedTile, Vector2.zero, Quaternion.identity) as GameObject;
                newTile.transform.parent = this.gameObject.transform;
                newTile.transform.localPosition = new Vector3(i, j);
            }
        }
    }
}
