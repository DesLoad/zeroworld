using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

class Chunk
{
    public string biome;
    public float[] heightNoise = new float[100];
}

public class NewNoiseGen : MonoBehaviour
{
    //public List<List<float>> perlinNoise = new List<List<float>>();

    Chunk[,] chunk = new Chunk[24, 100];

    string biomes;

    // Start is called before the first frame update
    void Start()
    {
        NoiseGen();
    }

    void NoiseGen()
    {
        for (int y = 0; y < 100; y++)
        {
            for (int x = 0; x < 24; x++)
            {
                switch (Random.Range(0,3))
                {
                    case 0:
                        {
                            chunk[x, y].biome = "Meadown";
                            break;
                        }
                    case 1:
                        {
                            chunk[x, y].biome = "Ocean";
                            break;
                        }
                    case 2:
                        {
                            chunk[x, y].biome = "Mountain";
                            break;
                        }
                }

                PerlinNoiseGen(x, y);
            }
        }
    }

    void PerlinNoiseGen(int xChunk, int yChunk)
    {
        if (chunk[xChunk, yChunk].biome == "Meadown") 
        {
            MeadowGen(xChunk, yChunk);
        }
        else if (chunk[xChunk, yChunk].biome == "Ocean")
        {
            OceanGen(xChunk, yChunk);
        }
        else if (chunk[xChunk, yChunk].biome == "Mountain")
        {
            MountainGen(xChunk, yChunk);
        }
    }

    void MeadowGen(int xChunk, int yChunk)
    {
        for (int i = 0; i * 10 < 100; i++ )
        { }
        //chunk[xChunk, yChunk].heightNoise =
    }

    void OceanGen (int xChunk, int yChunk)
    {

    }

    void MountainGen(int xChunk, int yChunk)
    {

    }

    void SmootheGenNoise(int xChunk, int yChunk)
    {



    }
}
