﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Threading;
using UniRx;
using System.Diagnostics;


using Debug = UnityEngine.Debug;

public class GenerateWorld : MonoBehaviour
{
    Stopwatch mywatch = new Stopwatch();

    public GameObject DirtTile;
    public GameObject StoneTile;
    public GameObject GrassTile;
    public GameObject WaterTile;

    public Vector2 prevCoord;
    public Vector2 nextCoord;


    [HideInInspector]
    public double seed;
    public int width;
    public string[] partSeed = new string[10];
    [HideInInspector]

    public static int worldSize = 1600;
    public static float worldCenterX = worldSize / 2;
    public int maxChunk = worldSize / 400;

    [HideInInspector]
    Vector2[] worldYPos = new Vector2[worldSize + 1];

    [HideInInspector]
    public int[] biomes = new int[16];

    [HideInInspector]
    public int[] height = new int[2000]; //высота генерации 

    void Start()
    {
        worldYPos[0] = new Vector2(0, 0);
        worldYPos[worldSize] = new Vector2(6400, 0);

        Generate();
    }

    //Сглаживатель
    void Smoothner(Vector2[] coordinatesBezie, int size, int numChank)
    {
        //Debug.Log(numChank);
        int xCor = numChank * 400;
        float highestX = coordinatesBezie[0].x;
        List<Vector2> worldPosition = new List<Vector2>();
        worldPosition.Clear();

        for (int i = 0; i <= size; i++)
        {
            if (Math.Truncate(coordinatesBezie[i].x) != Math.Truncate(coordinatesBezie[i + 1].x) && worldPosition.Count-1 != 400)
            {
                worldPosition.Add(new Vector2((int)coordinatesBezie[i].x, (int)coordinatesBezie[i].y));
            }
        }
        for (int i = 0; i < worldPosition.Count; i++)
        {
            worldYPos[xCor + i] = new Vector2(Mathf.RoundToInt(worldPosition[i].x),
                Mathf.RoundToInt(worldPosition[i].y));
            //BlockPlacer(DirtTile, Mathf.RoundToInt(worldPosition[i].x), Mathf.RoundToInt(worldPosition[i].y));
        }
        worldPosition.Clear();
    }

    void BlockCycle(int numChunk)
    {

        for (int i = 0; i < 400; i++)
        {
            BlockPlacer(GrassTile, numChunk * 400 + i, (int)worldYPos[numChunk * 400 + i].y);
            for (float h = worldYPos[numChunk*400 + i].y - 1; h > -200; h--)
                BlockPlacer(DirtTile, numChunk * 400 + i, (int)h);
        }
    }

    void BlockPlacer(GameObject selectedTile, int widthCoord, int heightCoord) //GameObject selectedTile, int widthCoord, int heightCoord
    {
        GameObject newTile = Instantiate(selectedTile, Vector3.zero, Quaternion.identity) as GameObject;
        newTile.transform.parent = this.gameObject.transform;
        newTile.transform.localPosition = new Vector2(widthCoord, heightCoord);
    }


    dynamic GenerateRock(Vector2 startPos)
    {
        List<Vector2> rockPos = new List<Vector2>();
        return rockPos;
    }

    void MeadowGen(int numChunk)
    {
        int pos = numChunk * 400;
        Vector2[] bezieCoord = new Vector2[2000];
        worldYPos[pos] = new Vector2(pos, worldYPos[pos].y);

        //Positions
        Vector2 p0 = worldYPos[pos];
        float randY = UnityEngine.Random.Range(-2f, 2f);
        Vector2 p1 = new Vector2(worldYPos[pos].x + 100f, worldYPos[pos].y + randY);
        randY = UnityEngine.Random.Range(-2f, 2f);
        Vector2 p2 = new Vector2(p1.x + 200f, p1.y + randY);
        randY = UnityEngine.Random.Range(-2f, 2f);
        Vector2 p3 = new Vector2(p2.x + 100f, p2.y + randY);

        if ((numChunk + 1) * 400 == worldSize)
        {
            p3 = worldYPos[worldSize];
        }
        else
            worldYPos[numChunk + 1 * 400] = p3;

        

        for (int w = 0; w <= 1000; w++)
        {
            double t = w * 0.001;

            bezieCoord[w] = Bezie.GetPoint4(p0, p1, p2, p3, (float)t);
        }
        Smoothner(bezieCoord, 1000, numChunk);
        bezieCoord = null;
    }

    void HillsGen(int numChunk)
    {
        ///<summary>Properties</summary>
        float maxHeight = 15f;
        float minHeight = 10f;


        int pos = numChunk * 400;
        Vector2[] bezieCoord = new Vector2[2500];
        worldYPos[pos] = new Vector2(pos, worldYPos[pos].y);

        //First part (200 = 50+100+50)
        Vector2 p10 = worldYPos[pos]; //Not changed

        float randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p11 = new Vector2(p10.x + 50f, randY);

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p12 = new Vector2(p11.x + 100f, p11.y + randY);

        randY = UnityEngine.Random.Range(-10f, -5f);
        Vector2 p13 = new Vector2(p12.x + 50f, p12.y + randY);

        for (int w = 0; w <= 1000; w++)
        {
            double t = w * 0.001;

            bezieCoord[w] = Bezie.GetPoint4(p10, p11, p12, p13, (float)t);
        }


        //Second part (400 = 50+100+50)
        Vector2 p20 = p13;

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p21 = new Vector2(p20.x + 50f, randY);

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p22 = new Vector2(p21.x + 100f, p21.y + randY);

        randY = UnityEngine.Random.Range(3f, 5f);
        Vector2 p23 = new Vector2(p22.x + 50f, randY);

        if ((numChunk + 1) * 400 == worldSize)
        {
            p23 = worldYPos[worldSize];
        }
        else
            worldYPos[numChunk + 1 * 400] = p23;



        //Рисует безье (Значение тут не менять лучше)
        for (int w = 0; w <= 1000; w++)
        {
            int w2 = w + 1000;
            double t = w * 0.001;

            bezieCoord[w2] = Bezie.GetPoint4(p20, p21, p22, p23, (float)t);
        }

        Smoothner(bezieCoord, 2000, numChunk); // размер нужен только для массива, он не влияет на координаты
    }

    void MountainsGen (int numChunk)
    {
        float maxHeight = 75f;
        float minHeight = 40f;


        int pos = numChunk * 400;
        Vector2[] bezieCoord = new Vector2[2500];
        worldYPos[pos] = new Vector2(pos, worldYPos[pos].y);

        //First part (100 = 40+20+40)
        Vector2 p10 = worldYPos[pos]; //Not changed

        float randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p11 = new Vector2(p10.x + 40f, randY);

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p12 = new Vector2(p11.x + 20f, p11.y + randY);

        randY = UnityEngine.Random.Range(5f, 10f);
        Vector2 p13 = new Vector2(p12.x + 40f, randY);

        for (int w = 0; w <= 500; w++)
        {
            double t = w * 0.002;

            bezieCoord[w] = Bezie.GetPoint3(p10, p12, p13, (float)t);
        }

        //Second part (200 = 75 + 50 +75)

        Vector2 p20 = p13;

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p21 = new Vector2(p20.x + 75f, p20.y + randY);

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p22 = new Vector2(p21.x + 50f, p21.y + randY);

        randY = UnityEngine.Random.Range(5f, 10f);
        Vector2 p23 = new Vector2(p22.x + 75f, randY);

        for (int w = 0; w <= 1000; w++)
        {
            int w2 = w + 500;
            double t = w * 0.001;

            bezieCoord[w2] = Bezie.GetPoint4(p20, p21, p22, p23, (float)t);
        }

        //Third part (100 = 40 + 20 + 40)
        Vector2 p30 = p23;

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p31 = new Vector2(p30.x + 40f, p30.y + randY);

        randY = UnityEngine.Random.Range(minHeight, maxHeight);
        Vector2 p32 = new Vector2(p31.x + 20f, p31.y + randY);

        randY = UnityEngine.Random.Range(5f, 10f);
        Vector2 p33 = new Vector2(p32.x + 40f, randY);

        if ((numChunk + 1) * 400 == worldSize)
        {
            p33 = worldYPos[worldSize];
        }
        else
            worldYPos[numChunk + 1 * 400] = p33;

        for (int w = 0; w <= 500; w++)
        {
            int w2 = w + 1500;
            double t = w * 0.002;

            bezieCoord[w2] = Bezie.GetPoint4(p30, p31, p32, p33, (float)t);
        }

        Smoothner(bezieCoord, 2000, numChunk);

    }

    void OceanGen(int numChunk)
    {
        float minDepth = -100f;
        float maxDepth = -50f;

        int pos = numChunk * 400;
        Vector2[] bezieCoord = new Vector2[2500];
        worldYPos[pos] = new Vector2(pos, worldYPos[pos].y);

        Vector2[] points = new Vector2[6];

        float randY;
        float randX;

        points[0] = (worldYPos[pos]); //1

        for (int i = 1; i < 5; i++)
        {
            randY = UnityEngine.Random.Range(minDepth, maxDepth);
            randX = UnityEngine.Random.Range(30, 60);
            points[i] = new Vector2(worldYPos[pos].x + 80*i + randX, randY); //2
        }

        randY = UnityEngine.Random.Range(3f, 10f);
        if ((numChunk + 1) * 400 == worldSize)
        {
            points[5] = (worldYPos[worldSize]);
        }
        else
            points[5] = (new Vector2(worldYPos[pos].x + 400, randY));


        for (int w = 0; w <= 1000; w++)
        {
            double t = w * 0.001;

            bezieCoord[w] = Bezie.GetPoint6(points, (float)t);
        }

        Smoothner(bezieCoord, 1000, numChunk);


    }

    void smoothChunk(int numChunk)
    {

        Vector2[] bezieCoord = new Vector2[1002];

        Vector2 p0 = prevCoord;
        Vector2 p1;
        if (prevCoord.y > nextCoord.y)
        {
            p1 = new Vector2(width / 2, (prevCoord.y - nextCoord.y) / 2 + nextCoord.y);
        }
        else
        {
            p1 = new Vector2(width / 2, (prevCoord.y - nextCoord.y) + nextCoord.y);
        }

        Vector2 p2 = nextCoord;

        for (int w = 0; w <= 1000; w++)
        {
            double t = w * 0.001;

            bezieCoord[w] = Bezie.GetPoint3(p0, p1, p2, (float)t);
        }

        Smoothner(bezieCoord, 1000, numChunk);
    }

    //void GenerateOcean()
    //{
    //    Vector2[] bezieCoord = new Vector2[2000];

    //    for (int w = 0; w <= 1000; w++)
    //    {
    //        double t = w * 0.001;

    //        bezieCoord[w] = Bezie.GetPoint4(new Vector2(0, -50), new Vector2(100, -45),
    //            new Vector2(150, -20), new Vector2(175, 20), (float)t);
    //    }
    //    //Smoothner(bezieCoord, 1000);
    //}



    //Методы передает точки вершин для метода Bezie.Getpoint
    void GenerateTerrain()
    {
        for (int i = 0; i < maxChunk; i++)
        {
            if (biomes[i] <= 2 && biomes[i] >= 0) // meadow
            {
                Debug.Log("Meadow");
                MeadowGen(i);
            }
            else if (biomes[i] <= 4 && biomes[i] >= 2) // hills 
            {
                Debug.Log("Hills");
                HillsGen(i);
            }
            else if (biomes[i] >= 5 && biomes[i] <= 6) // Montains
            {
                Debug.Log("mountains");
                MountainsGen(i);
            }
            else //Ocean
            {
                Debug.Log("Ocean");
                OceanGen(i);
            }
            smoothChunk(i);
        }


        mywatch.Start();
        //Расстановка блоков
        for (int i = 0; i < maxChunk; i++)
        {
            BlockCycle(i);
        }
        mywatch.Stop();
        Debug.Log(mywatch.Elapsed);

    }

    void WorldCut()
    {
        for (int i = 0; i < worldSize/400 ; i++)
        {
            int rand = UnityEngine.Random.Range(0, 10);
            biomes[i] = rand;
        }
    }

    void Generate()
    {

        WorldCut();

        GenerateTerrain();

    }
}