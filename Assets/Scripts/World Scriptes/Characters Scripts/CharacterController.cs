﻿using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [HideInInspector] public static GameObject classGO;
    public float maxSpeed = 10f;        //скорость персонажа
    public bool isFacingRight = true;   // сторона поворота
    public float jumpForce = 20;        // сила прыжка

    public static string charClass = "Warrior";
    private bool isGrounded = true;     //Персонаж стоит
    public Transform groundCheck;       //Проверяющий объект
    public float checkRadius;           //Радиус проверки
    public LayerMask whatIsGround;      //то что является основанием

    private int extraJumps;              //Кол-во доп прыжков
    public int extraJumpsValue;          //

    private Animator characterAnimation;
    private Rigidbody2D rb;

    GameObject character;

    private void Start()
    {
        //charClass = GameObject.Find("Class");
        character = GameObject.Find("Character");
        character.GetComponent<SpriteRenderer>().sprite = Resources.Load
            ("Assets/Sprites/CharacterSprites/" + charClass + "/$" + charClass + "_7") as Sprite;
        character.gameObject.GetComponent<Animator>().runtimeAnimatorController = Resources.Load 
            ("Assets/Sprites/CharacterSprites/" + charClass + "/" + charClass) as RuntimeAnimatorController;


        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();
        characterAnimation = GetComponent<Animator>();
    }

    private void Move()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        //при стандартных настройках проекта 
        //-1 возвращается при нажатии на клавиатуре стрелки влево (или клавиши А),
        //1 возвращается при нажатии на клавиатуре стрелки вправо (или клавиши D)
        float move = Input.GetAxis("Horizontal");


        //в компоненте анимаций изменяем значение параметра Speed на значение оси Х.
        //приэтом нам нужен модуль значения
        characterAnimation.SetFloat("Speed", Mathf.Abs(move));

        //обращаемся к компоненту персонажа RigidBody2D. задаем ему скорость по оси Х, 
        //равную значению оси Х умноженное на значение макс. скорости
        rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);

        //если нажали клавишу для перемещения вправо, а персонаж направлен влево
        // Обработка текстур
        if (move > 0 && !isFacingRight)
            //отражаем персонажа вправо
            Flip();
        //обратная ситуация. отражаем персонажа влево
        else if (move < 0 && isFacingRight)
            Flip();
    }

    private void FixedUpdate()
    {
        Move();

        //if (Input.GetMouseButtonDown (0))
        //{
        //    Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //    RaycastHit2D hit2d = Physics2D.Raycast(transform.position, mousePos = transform.position);

        //    if (hit2d.collider.gameObject != null)
        //    {
        //        Destroy(hit2d.collider.gameObject);
        //    }
        //}

    }

    private void Update()
    {
        if (isGrounded == true)
        {
            extraJumps = extraJumpsValue;
        }

        if (Input.GetKeyDown(KeyCode.Space) && (extraJumps > 0))
        {
            rb.velocity = Vector2.up * jumpForce;//Прыжок по вектору с определенной силой
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    private void Flip()
    {
        //меняем направление движения персонажа
        isFacingRight = !isFacingRight;
        //получаем размеры персонажа
        Vector3 theScale = transform.localScale;
        //зеркально отражаем персонажа по оси Х
        theScale.x *= -1;
        //задаем новый размер персонажа, равный старому, но зеркально отраженный
        transform.localScale = theScale;
    }

}
