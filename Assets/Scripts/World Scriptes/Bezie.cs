using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Bezie
{
    public static Vector2 GetPoint4(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t)
    {
        Vector2 p01 = Vector2.Lerp(p0, p1, t);
        Vector2 p12 = Vector2.Lerp(p1, p2, t);
        Vector2 p23 = Vector2.Lerp(p2, p3, t);

        Vector2 p012 = Vector2.Lerp(p01, p12, t);
        Vector2 p123 = Vector2.Lerp(p12, p23, t);

        Vector2 p0123 = Vector2.Lerp(p012, p123, t);

        return p0123;
    }

    public static Vector2 GetPoint3(Vector2 p0, Vector2 p1, Vector2 p2, float t)
    {
        Vector2 p01 = Vector2.Lerp(p0, p1, t);
        Vector2 p12 = Vector2.Lerp(p1, p2, t);

        Vector2 p012 = Vector2.Lerp(p01, p12, t);

        return p012;
    }

    public static Vector2 GetPoint6(Vector2[] points, float t)
    {
        Vector2 p01 = Vector2.Lerp(points[0], points[1], t);
        Vector2 p12 = Vector2.Lerp(points[1], points[2], t);
        Vector2 p23 = Vector2.Lerp(points[2], points[3], t);
        Vector2 p34 = Vector2.Lerp(points[3], points[4], t);
        Vector2 p45 = Vector2.Lerp(points[4], points[5], t);

        Vector2 p012 = Vector2.Lerp(p01, p12, t);
        Vector2 p123 = Vector2.Lerp(p12, p23, t);
        Vector2 p234 = Vector2.Lerp(p23, p34, t);
        Vector2 p345 = Vector2.Lerp(p34, p45, t);

        Vector2 p0123 = Vector2.Lerp(p012, p123, t);
        Vector2 p1234 = Vector2.Lerp(p123, p234, t);
        Vector2 p2345 = Vector2.Lerp(p234, p345, t);

        Vector2 p04 = Vector2.Lerp(p0123, p1234, t);
        Vector2 p15 = Vector2.Lerp(p1234, p2345, t);

        Vector2 p = Vector2.Lerp(p04, p15, t);

        return p;
    }
}
