using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;


using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;


public class Chunks : MonoBehaviour
{
    Stopwatch mywatch = new Stopwatch();

    public GameObject chunk;

    public int chunkWidth = 200;

    [HideInInspector]
    public int biomes;
    [HideInInspector]
    public int numChunks = 8;
    [HideInInspector]
    public float seed;
    // Start is called before the first frame update
    void Start()
    {
        seed = Random.Range(-200f, 200f);
        Generate();
    }

    // Update is called once per frame
    void Generate()
    {
        float heightMultiplier = 10f;
        heightMultiplier = heightMultiplier + Random.Range(-1f, 1f);
        int lastX = -chunkWidth * (numChunks / 2 + 1);
        for (int i = 0; i < numChunks; i++)
        {
            GameObject newChunk = Instantiate(chunk, new Vector3(lastX + chunkWidth, 0f), Quaternion.identity) as GameObject;
            newChunk.transform.parent = this.gameObject.transform;
            newChunk.GetComponent<GenerateChunk>().biome = Random.Range(0, 5);
            newChunk.GetComponent<GenerateChunk>().seed = seed;
            newChunk.GetComponent<GenerateChunk>().heightMultiplier = heightMultiplier;
            newChunk.GetComponent<GenerateChunk>().numChunk = i;
            newChunk.GetComponent<GenerateChunk>().width = chunkWidth;
            lastX += chunkWidth;
        }
    }
}
