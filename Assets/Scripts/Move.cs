using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteAlways]
public class Move : MonoBehaviour
{
    public Transform p0;
    public Transform p1;
    public Transform p2;
    public Transform p3;

    [Range(0, 1)]
    public float t;
    // Update is called once per frame
    void Update()
    {
        transform.position = Bezie.GetPoint4(p0.position, p1.position, p2.position, p3.position, t);
    }
}
