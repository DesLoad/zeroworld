using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Extensions;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine.Assertions;
using UnityEngine.UI;
using MLAPI.Serialization;

public struct Score
{
    public ulong ID;
    public int kills;
    public int deaths;
}

public class StatsRegistr : NetworkBehaviour
{
    
}
