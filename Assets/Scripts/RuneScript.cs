using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Extensions;
using MLAPI.NetworkVariable;
using UnityEngine.Assertions;


public class RuneScript : NetworkBehaviour
{
    static string s_ObjectPoolTag = "ObjectPool";
    public Camera cam;
    public static int numPowerups = 0;

    NetworkObjectPool m_ObjectPool;

    public NetworkVariable<Buff.BuffType> buffType = new NetworkVariable<Buff.BuffType>();

    private void Awake()
    {
        Debug.Log("Check pool");
        m_ObjectPool = GameObject.FindWithTag(s_ObjectPoolTag).GetComponent<NetworkObjectPool>();
        Assert.IsNotNull(m_ObjectPool, $"{nameof(NetworkObjectPool)} not found in scene. Did you apply the {s_ObjectPoolTag} to the GameObject?");
    }

    public override void NetworkStart()
    {
        if (IsClient)
        {
            OnStartClient();
        }

        if (IsServer)
        {
            OnStartServer();
        }
    }

    void OnStartClient()
    {
        //float dir = 170.0f;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        GetComponent<Rigidbody2D>().angularVelocity = 0;

        Color color = Buff.bufColors[(int)buffType.Value];
        GetComponent<Renderer>().material.color = color;

        if (!IsServer)
        {
            numPowerups += 1;
        }
    }

    void OnStartServer()
    {
        numPowerups += 1;
    }

    void OnGUI()
    {
        GUI.color = Buff.bufColors[(int)buffType.Value];
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Label(new Rect(pos.x - 20, Screen.height - pos.y - 30, 100, 30), buffType.Value.ToString());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsServer)
        {
            return;
        }

        var otherServerPlayer = other.gameObject.GetComponent<ServerPlayer>();
        if (otherServerPlayer != null)
        {
            Debug.Log("You touch buff");
            otherServerPlayer.AddBuff(buffType.Value);
            DestroyPowerUp();
        }
    }

    void DestroyPowerUp()
    {
        //AudioSource.PlayClipAtPoint(GetComponent<AudioSource>().clip, transform.position);
        numPowerups -= 1;

        NetworkObject.Despawn();
        m_ObjectPool.ReturnNetworkObject(NetworkObject);
    }
}
