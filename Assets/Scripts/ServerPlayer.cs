using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Extensions;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using MLAPI.Serialization;

public class Buff
{
    public enum BuffType
    {
        Speed,
        Health,
        QuadDamage,
        Bounce,
        Last
    };

    public static Color[] bufColors = { Color.red, Color.blue, Color.cyan, Color.yellow, Color.green, Color.magenta, new Color(1, 0.5f, 0), new Color(0, 1, 0.5f) };

    public static Color GetColor(BuffType bt)
    {
        return bufColors[(int)bt];
    }
}


public class ServerPlayer : NetworkBehaviour
{
    public float maxSpeed = 10f;        //�������� ���������
    public bool isFacingRight = true;   // ������� ��������
    public float jumpForce = 20;        // ���� ������
    private int m_Damage = 20;

    public static string charClass = "Warrior";
    private bool isGrounded = true;     //�������� �����
    public Transform groundCheck;       //����������� ������
    public float checkRadius;           //������ ��������
    public LayerMask whatIsGround;      //�� ��� �������� ����������

    private int extraJumps;              //���-�� ��� �������
    public int extraJumpsValue;          //
    public int bounceJumpsValue;
    public Vector3 latestPos = new Vector3(0, 0);

    public NetworkManager client;

    static string s_ObjectPoolTag = "ObjectPool";

    NetworkObjectPool m_ObjectPool;
    GameObject pool;
    public GameObject BulletPrefab;

    private Animator characterAnimation;
    private Rigidbody2D rb;
    [SerializeField]public GameObject scoreGO;
    [SerializeField] public GameObject nameGO;
    [SerializeField] public GameObject canvasGO;
    public GameObject hpInfo;
    public GameObject gameUI;

    //Buffs Timer
    public NetworkVariableFloat SpeedBuffTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat RotateBuffTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat TripleShotTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat DoubleShotTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat QuadDamageTimer = new NetworkVariableFloat(0);
    public NetworkVariableFloat BounceTimer = new NetworkVariableFloat(0);



    //Network player
    public NetworkVariableInt Health = new NetworkVariableInt(100);
    public NetworkVariableInt Energy = new NetworkVariableInt(100);


    float rotateSpeed = 200f;
    float acceleration = 12f;
    float bulletLifetime = 3;
    float topSpeed = 7.0f;
    [SerializeField] private int m_Deaths = 0;
    [SerializeField] private int m_Kills = 0;


    public NetworkVariableString PlayerName = new NetworkVariableString("SomeGuy");



    public NetworkVariableVector3 Position = new NetworkVariableVector3(new NetworkVariableSettings
    {
        WritePermission = NetworkVariablePermission.OwnerOnly,
        ReadPermission = NetworkVariablePermission.Everyone
    });

    void Awake()
    {
        transform.position = new Vector2(Random.Range(-5f, 5f), 3);

        pool = GameObject.FindWithTag(s_ObjectPoolTag);
        var list = pool.GetComponents(typeof(Component));
        for (int i = 0; i < list.Length; i++)
        {
            Debug.Log(list[i].name);
        }
        m_ObjectPool = GameObject.FindWithTag(s_ObjectPoolTag).GetComponent<NetworkObjectPool>();
        if (m_ObjectPool == null)
            Debug.Log(pool.GetComponents<NetworkObjectPool>());
        Assert.IsNotNull(m_ObjectPool, $"{nameof(NetworkObjectPool)} not found in scene. Did you apply the {s_ObjectPoolTag} to the GameObject?");

        hpInfo.GetComponent<Text>().text = Health.Value.ToString();
        nameGO.GetComponent<Text>().text = name;
        scoreGO.GetComponent<Text>().text = m_Kills.ToString() + "/" + m_Deaths.ToString();
    }

    public override void NetworkStart()
    {
        transform.position = new Vector3(0, 1);
    }

    private void FixedUpdate()
    {
        if (IsLocalPlayer)
        {
            MovePlayer();
        }
        canvasGO.GetComponent<Canvas>().transform.eulerAngles = new Vector3(0, 0, 0);
    }

    private void Update()
    {
        if (IsLocalPlayer)
        {
            JumpPlayer();

            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                FireServerRpc(isFacingRight);
            }

            hpInfo.GetComponent<Text>().text = Health.Value.ToString();
            scoreGO.GetComponent<Text>().text = m_Kills.ToString() + "/" + m_Deaths.ToString();
        }
        ScoreClientRpc();
        this.scoreGO.GetComponent<Text>().text = this.m_Kills.ToString() + "/" + this.m_Deaths.ToString();
    }

    void MovePlayer()
    {
        if (SpeedBuffTimer.Value > Time.time)
        {
            maxSpeed = 20f;
        }
        else
            maxSpeed = 10f;
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        float move = Input.GetAxis("Horizontal");

        //characterAnimation.SetFloat("Speed", Mathf.Abs(move));

        rb.velocity = new Vector2(move * maxSpeed, rb.velocity.y);

        if (move > 0 && !isFacingRight)
            Flip();
        else if (move < 0 && isFacingRight)
            Flip();
    }

    void JumpPlayer()
    {
        if (isGrounded == true)
        {
            if (BounceTimer.Value > Time.time)
            {
                extraJumps = bounceJumpsValue;
            }
            else
            {
                extraJumps = extraJumpsValue;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && (extraJumps > 0))
        {
            rb.velocity = Vector2.up * jumpForce;//������ �� ������� � ������������ �����
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    void Fire(bool isFacingRight)
    {
        if (QuadDamageTimer.Value > Time.time)
        {
            m_Damage = 50;
        }
        else
            m_Damage = 20;

        bool bounce = BounceTimer.Value > Time.time;

        GameObject bullet = m_ObjectPool.GetNetworkObject(BulletPrefab);
        var bulletRb = bullet.GetComponent<Rigidbody2D>();
        bulletRb.transform.position = NetworkObject.transform.position;
        Vector3 rotation;
        if (isFacingRight == true)
        {
            bullet.transform.position = new Vector2(bulletRb.transform.position.x + 2, transform.position.y);
            rotation = new Vector3(0, 0, -45);
        }
        else
        {
            bullet.transform.position = new Vector2(bulletRb.transform.position.x - 2, transform.position.y);
            rotation = new Vector3(0, 0, -225);
        }

        Vector2 velocity;

        velocity = rb.transform.right;
        velocity += velocity * 50;
        bulletRb.velocity = velocity;
        bulletRb.transform.eulerAngles = rotation;
        bullet.GetComponent<Bullet>().Config(isFacingRight, this, m_Damage, bounce, bulletLifetime, m_ObjectPool);

        bullet.GetComponent<NetworkObject>().Spawn(null, true);
    }

    public bool TakeDamage(int amount)
    {
        Health.Value = Health.Value - amount;
        hpInfo.GetComponent<Text>().text = Health.Value.ToString();
        if (Health.Value <= 0)
        {
            Health.Value = 0;

            //todo: reset all buffs
            Health.Value = 100;
            //transform.position = new Vector2(0, 3);
            //GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            //GetComponent<Rigidbody2D>().angularVelocity = 0;
            return true;
        }
        return false;
    }

    void OnHealth(int oldValue, int newValue)
    {
        Health.Value = newValue;
    }

    void OnEnable()
    {
        Health.OnValueChanged += OnHealth;
    }

    void OnDisable()
    {
        Health.OnValueChanged -= OnHealth;
    }

    private void Start()
    {
        //GetComponent<NetworkObject>().ChangeOwnership(ID);
        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();

        if (IsLocalPlayer)
        {
            gameUI.SetActive(true);
        }
    }

    private void Flip()
    {
        if (!IsLocalPlayer)
        {
            return;
        }
        if (isFacingRight == false)
        {
            //canvasGO.GetComponent<Canvas>().transform.eulerAngles = new Vector3(0, 180, 0);
            rb.transform.eulerAngles = new Vector3(0, 0, 0);
            isFacingRight = true;
        }
        else
        {
            //canvasGO.GetComponent<Canvas>().transform.eulerAngles = new Vector3(0, 180, 0);
            rb.transform.eulerAngles = new Vector3(0, 180, 0);
            isFacingRight = false;
        }
        //isFacingRight = !isFacingRight;

        /*
        //������ ����������� �������� ���������
        
        //�������� ������� ���������
        theScale = rb.transform.eulerAngles;
        //��������� �������� ��������� �� ��� �
        theScale.y *= -1;
        //������ ����� ������ ���������, ������ �������, �� ��������� ����������
        rb.transform.eulerAngles = theScale;*/
    }

    public void AddBuff(Buff.BuffType buff)
    {
        if (buff == Buff.BuffType.Speed)
        {
            SpeedBuffTimer.Value = Time.time + 10;
        }

        if (buff == Buff.BuffType.Health)
        {
            Health.Value += 20;
            if (Health.Value >= 100)
            {
                Health.Value = 120;
            }
        }

        if (buff == Buff.BuffType.QuadDamage)
        {
            QuadDamageTimer.Value = Time.time + 10;
        }

        if (buff == Buff.BuffType.Bounce)
        {
            BounceTimer.Value = Time.time + 10;
            Debug.Log("Bounce");
        }
    }
    public string ScoreReturn()
    {
        Debug.Log(m_Kills + "/" + m_Deaths);
        return (m_Kills + "/" + m_Deaths);
    }

    public void ScorePlus()
    {
        if (IsLocalPlayer)
        {
            m_Kills++;

            scoreGO.GetComponent<Text>().text = m_Kills.ToString() + "/" + m_Deaths.ToString();
        }
    }

    public void DeathPlus()
    {
        if (IsLocalPlayer)
        {
            m_Deaths++;

            scoreGO.GetComponent<Text>().text = m_Kills.ToString() + "/" + m_Deaths.ToString();
        }
    }

    //ClientRPC

    [ClientRpc]
    public void ScoreClientRpc()
    {
        scoreGO.GetComponent<Text>().text = m_Kills.ToString() + "/" + m_Deaths.ToString();
    }

    //ServerRPC


    [ServerRpc]
    void FireServerRpc(bool facing)
    {
        Fire(facing);
    }

}
