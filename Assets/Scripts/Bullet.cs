using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Extensions;
using MLAPI.Messaging;
using UnityEngine.UI;

public class Bullet : NetworkBehaviour
{
    // Start is called before the first frame update
    NetworkObjectPool m_PoolToReturn;
    bool m_Bounce = false;
    private int m_Damage;
    ServerPlayer m_Owner;
    private Rigidbody2D rb;
    bool facing;

    public GameObject explosionParticle;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Config(bool facing, ServerPlayer owner, int damage, bool bounce, float lifetime, NetworkObjectPool poolToReturn)
    {
        m_Owner = owner;
        m_Damage = damage;
        m_Bounce = bounce;
        m_PoolToReturn = poolToReturn;

        if (IsServer)
        {
            // This is bad code don't use invoke.
            Invoke(nameof(DestroyBullet), lifetime);
        }
        Debug.Log(facing);
    }

    private void DestroyBullet()
    {
        GameObject ex;
        if (!NetworkObject.IsSpawned)
        {
            return;
        }

        Vector3 pos = transform.position;
        //pos = new Vector3(pos.x + 1, pos.y + 1, pos.z);
        ex = Instantiate(explosionParticle, pos, Quaternion.identity);

        Destroy(ex, 0.5f);

        NetworkObject.Despawn();
        m_PoolToReturn.ReturnNetworkObject(NetworkObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!NetworkManager.Singleton.IsServer || !NetworkObject.IsSpawned)
        {
            return;
        }

        if (other.gameObject.layer == 8 || other.gameObject.layer == 6)
        {
            if (m_Bounce == true)
            {
                Vector2 velocity = rb.transform.right;
                if (facing == true)
                {
                    transform.eulerAngles = new Vector3(0, 0, -225);
                }
                else
                    transform.eulerAngles = new Vector3(0, 0, -45);

                velocity += velocity * 50;
                rb.velocity = velocity;
            }
            else
                DestroyBullet();
        }

        var player = other.gameObject.GetComponent<ServerPlayer>();
        if (player != null)
        {
            if (player != m_Owner)
            {
                if (NetworkManager.gameObject.tag == "OneShot")
                    m_Damage = 100;

                //Death = true
                if (player.TakeDamage(m_Damage) == true)
                {
                    Debug.Log("I'm here");

                    player.transform.position = new Vector2(Random.Range(-35f, 35f), Random.Range(2f, 10f));

                    m_Owner.ScorePlus();
                    player.DeathPlus();
                    m_Owner.scoreGO.GetComponent<Text>().text = m_Owner.ScoreReturn();
                    Debug.Log(m_Owner.scoreGO.GetComponent<Text>().text);
                    player.scoreGO.GetComponent<Text>().text = player.ScoreReturn();
                    Debug.Log(player.scoreGO.GetComponent<Text>().text);
                }
                DestroyBullet();
            }
        }
    }
}
