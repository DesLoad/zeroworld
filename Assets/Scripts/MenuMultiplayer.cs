using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Configuration;
using MLAPI.Transports.UNET;
using MLAPI.Extensions;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using MLAPI.Serialization;
using MLAPI.Connection;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MenuMultiplayer : NetworkBehaviour
{
    public GameObject menuPanel;
    public GameObject disconnectButton;
    public GameObject modeButton;
    public InputField inputField;

    public GameObject shieldGO;
    public GameObject archerGO;

    public GameObject managerGO;
    private NetworkManager manager;

    public GameObject classApply;
    private string tagObj;
    private GameObject go;

    private void Start()
    {
        manager = managerGO.GetComponent<NetworkManager>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {

        }
    }

    public void ServerClick()
    {
        disconnectButton.SetActive(true);
        NetworkManager.Singleton.StartServer();

        menuPanel.SetActive(false);
    }

    public void ConnectClick()
    {
        disconnectButton.SetActive(true);
        //Clicked Join
        if (inputField.text.Length <= 0)
        {
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = "127.0.0.1";
        }
        else
        {
            NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = inputField.text;
        }
        NetworkManager.Singleton.StartClient();

        menuPanel.SetActive(false);
    }

    [ClientRpc]
    public void ClientSpawnClientRpc(ulong ID)
    {
        if (tagObj == "Archer")
        {
            go = NetworkManager.Singleton.NetworkConfig.NetworkPrefabs[0].Prefab;
        }
        else
        {
            go = NetworkManager.Singleton.NetworkConfig.NetworkPrefabs[1].Prefab;
        }
        go.GetComponent<NetworkObject>().Spawn();
    }

    public void HostClick()
    {
        disconnectButton.SetActive(true);
        Debug.Log("1");
        NetworkManager.Singleton.StartHost();
        //Debug.Log("2");
        //if (classApply.tag == "Archer")
        //{
        //    Debug.Log("2.1");
        //    go = Instantiate(archerGO, Vector3.zero, Quaternion.identity);
        //}
        //else
        //{
        //    Debug.Log("2.1");
        //    go = Instantiate(shieldGO, Vector3.zero, Quaternion.identity);
        //}
        //Debug.Log("3");
        //go.GetComponent<NetworkObject>().SpawnAsPlayerObject(manager.LocalClientId);
        ////go.GetComponent<NetworkObject>().Spawn();
        ////go.GetComponent<NetworkObject>().ChangeOwnership(NetworkManager.ServerClientId);
        //Debug.Log("4");
        menuPanel.SetActive(false);
    }

    public void StopClient()
    {
        disconnectButton.SetActive(false);
        menuPanel.SetActive(true);
        if (IsClient)
        {
            NetworkManager.Singleton.StopClient();
            return;
        }
        if (IsHost)
        {
            NetworkManager.Singleton.StopHost();
            return;
        }
        if (IsServer)
        {
            NetworkManager.Singleton.StopServer();
            return;
        }
    }

    public void CharacterButton()
    {
        menuPanel.SetActive(false);
        classApply.SetActive(true);
    }

    public void SwitchMode()
    {
        if (this.gameObject.tag == "Simple")
            this.gameObject.tag = "OneShot";
        else
            this.gameObject.tag = "Simple";

        modeButton.GetComponentInChildren<Text>().text = "Mode:" + "\n" + this.gameObject.tag;
    }

    private void OnConnectedToServer()
    {
        if (IsServer)
        {

        }
    }

}
